import pandas
import numpy


def height_weight_handler(file_name):

    h = '"Height(Inches)"'
    w = '"Weight(Pounds)"'

    inches_to_cm = 2.54
    pounds_to_kg = 2.2
    default_decimal_point_step = 1

    column_names = [h, w]
    data = pandas.read_csv(file_name, names=column_names)

    height, weight = data[h].to_list(), data[w].to_list()

    del height[0]
    del weight[0]

    height, weight = [float(i) for i in height], [float(i) for i in weight]

    # print(height, "\n ", weight)

    mean_height, mean_weight = numpy.mean(height), numpy.mean(weight)

    # print(mean_height, mean_weight)

    # inches to cm
    final_height = round(mean_height * inches_to_cm, default_decimal_point_step)
    # pounds to kg
    final_weight = round(mean_weight / pounds_to_kg, default_decimal_point_step)

    return f'From CSV file "{file_name}": \nmean height value is {final_height} kg; \nmean weight value is ' \
           f'{final_weight} pounds'


height_weight_handler("hw.csv")
