

def parse_int(request, param_name, default, min=float('-inf'), max=float('inf')):
    value = request.args.get(param_name, str(default))

    if not value.isdigit():
        raise ValueError(f'Error: int is expected for {param_name} param')

    value = int(value)

    if not (min <= value <= max):
        raise ValueError(f'Error: {param_name} is out of range: should be between {min} and {max}')

    return value


def parse_bool(request, param_name, default):
    value = request.args.get(param_name, str(int(default)))

    if not value.isdigit():
        raise ValueError(f'Error: int is expected for {param_name} param')

    value = int(value)

    if not (0 <= value <= 1):
        raise ValueError(f'Error: {param_name} should be boolean (0 or 1)')

    return value

