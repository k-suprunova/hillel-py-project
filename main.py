import datetime
import random
from flask import Response
import string
from utils import parse_int, parse_bool
from csv_hw import height_weight_handler
from flask import Flask, request
import flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    """
    returns HW when requesting root route
    """
    return 'Hello, World!!!'


@app.route('/now')
def now():
    """
    returns date and time of the moment when requesting this route
    """
    return str(datetime.datetime.now())


@app.route('/random')
def get_random():
    """
    returns random character mix when requesting /random with passed 'length' and optional 'digits' and 'specials' args
    """

    try:
        length = parse_int(request, 'length', 10, min=1, max=100)
        digits = parse_bool(request, 'digits', False)
        specials = parse_bool(request, 'specials', False)
    except ValueError as ex:
        return Response(str(ex), status=400)

    fin_str = string.ascii_lowercase

    if digits:
        fin_str += string.digits

    if specials:
        fin_str += string.punctuation

    result = ''
    for i in range(length):

        result += random.choice(fin_str)

    return flask.escape(result)


@app.route('/requirements')
def get_requirements():
    """
    returns requirements.txt file content when requesting this route
    """
    with open('requirements.txt') as rf:
        req_list = []
        for line in rf:
            req_list.append(line)
    return str(req_list)


@app.route('/avr_data')
def get_avr_data():
    """
    returns average data from hw.csv file // result of height_weight_handler function from csv_hw.py
    """
    return str(height_weight_handler("hw.csv"))


app.run()

